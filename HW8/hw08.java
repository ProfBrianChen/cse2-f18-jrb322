//Jeffrey Barrett - Homework 08 - shuffling cards - Due 11/15/18

//import scanner and random
import java.util.Scanner;
import java.util.Random;

public class hw08 {
	
	//initialize random function to be used outside the main method
	static Random random = new Random();

	public static void main(String[] args) {
		
		//initialize scanner function
		Scanner scanner = new Scanner(System.in);
		
		//suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		
		//string array for cards and hand
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		
		//declare and initialize variables
		int numCards = 0; 
		int again = 1; 
		int index = 51;
		
		//printing / calling functions to make the program run
		System.out.println("Ordered cards");
		printArray(cards, rankNames, suitNames);
		System.out.println();
		shuffle(cards, rankNames, suitNames); 
		System.out.println();
		//if the user wants to play again, call the functions
		while(again == 1){ 
		   hand = getHand(cards,index,numCards); 
		   printArray(hand, hand, hand);
		   index = index - numCards;
		   System.out.println();
		   System.out.println("Enter a 1 if you want another hand drawn"); 
		   again = scanner.nextInt(); 
		}
	}
	
	//method to shuffle the cards
	public static void shuffle(String[] cards, String[] rankNames, String[] suitNames) {
		
		//use temp variable to switch elements in the array and randomize the order of the cards
		String temp = "0";
		System.out.println("Shuffled");
		for(int i = 0; i < 52; i++) {
			int rand = random.nextInt(52);
			temp = cards[rand];
			cards[rand] = cards[0];
			cards[0] = temp;
			System.out.print(temp+" ");
		}
	}

	//method to get a hand of cards out of the 52 randomized cards
	public static String[] getHand(String[] cards, int index, int cardNum) {

		System.out.println("Hand");
		return cards;
		
	}
	
	//method to print the array (the deck of cards)
	public static void printArray(String[] cards, String[] rankNames, String[] suitNames) {
		for (int i=0; i<52; i++){ 
			cards[i]=rankNames[i%13]+suitNames[i/13]; 
			System.out.print(cards[i]+" "); 
		}
	}
}