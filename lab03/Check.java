//import the scanner
import java.util.Scanner;

public class Check {
  public static void main(String[] args) {
    
//Uses the Scanner class to obtain from the user the original cost of the check,
//the percentage tip they wish to pay, and the number of ways the check will be split.
//Then determine how much each person in the group needs to spend in order to pay the check
    
//create a scanner
    Scanner myScanner = new Scanner(System.in);
    
//asl the user to enter a double
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    
//ask the user to enter another double
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
    double tipPercent = myScanner.nextDouble();
    
//convert number to a decimal
    tipPercent = tipPercent / 100; //We want to convert the percentage into a decimal value
   
//ask the user to input an int
    System.out.print("enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
   
//create variabels to later calculate the cost, tip, etc... 
    double totalCost;
    double costPerPerson;
    int dollars,dimes, pennies; //for storing digits to the right of the decimal point for the cost$ 
    
//begin making calculations
//get the whole amount, dropping decimal fraction
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars=(int)costPerPerson;
    
//find the values we need and then output them
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $ " + dollars + "." + dimes + pennies);
  }
}