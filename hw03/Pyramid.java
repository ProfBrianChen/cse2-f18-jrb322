import java.util.Scanner;

public class Pyramid {
  public static void main(String[] args) {
    
//Write a program that prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("What is the length of the pyramid?");
    double length = myScanner.nextDouble();
    
    System.out.println("What is the height of the pyramid?");
    double height = myScanner.nextDouble();
    
    double volume = (length * length * height) / 3;
    System.out.println("The volume is " + volume);
  }
}