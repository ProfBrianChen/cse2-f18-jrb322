import java.util.Scanner;

public class Convert {
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
//Write a program that asks the user for doubles that represent the number of acres of land 
//affected by hurricane precipitation and how many inches of rain were dropped on average
//Convert the quantity of rain into cubic miles.
    
//getting input from the user and storing the data in a variable
    System.out.println("How many acres of land were in the area affected by the hurricane?");
    double numAcres = myScanner.nextDouble();
    
//getting input from the user and storing the data in a variable
    System.out.println("How many inches of rain were dropped on average?");
    double inchesOfRain = myScanner.nextDouble();
    
//Convert the quantity of rain into cubic miles.
    //convert acre-inch to gallons
    //convert gallons to cubic miles
    double gallons = numAcres * inchesOfRain * 27154.285990761;
    double cubicMiles = gallons * 9.08169e-13;
    
//final print statements for the answers
    System.out.println();
    System.out.println("The number of acres of land affected by the hurricane was " + numAcres);
    System.out.println("The average amount of rain dropped measured in inches was " + inchesOfRain);
    System.out.println("The final answer is " + cubicMiles + " cubic miles");
  }
}