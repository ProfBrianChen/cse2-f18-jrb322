//Jeffrey Barrett CSE 002, Hw 07 - Edited Text - Started 10/27/18
 
//import scanner
import java.util.Scanner;

public class wordTools {
	
	//initialize scanner
	static Scanner scanner = new Scanner(System.in);

	//main method (calling functions / checking user input
	public static void main(String[] args) {
		
		//calling first two functions
		String text = sampleText();
		
		String userInput = printMenu();
		
		//calling functions if that letter is inputed
		if(userInput.equals("c")) {
			getNumOfNonWSCharacters(text);
      printMenu();
		}
		
		else if(userInput.equals("w")) {
			getNumOfWords(text);
      printMenu();
		}
		
		else if(userInput.equals("f")) {
			findText("jeffrey",text);
      printMenu();
		}
		
		else if(userInput.equals("r")) {
			String newText = replaceExclamation(text);
			System.out.println(newText);
      printMenu();
		}
		
		else if(userInput.equals("s")) {
			String newText = shortenSpace(text);
			System.out.println(newText);
      printMenu();
		}
		
		//checking to make sure correct character is inputed
		while(!userInput.equals("c") || !userInput.equals("w") || !userInput.equals("f") || !userInput.equals("r") || !userInput.equals("s") || !userInput.equals("q")) {
			userInput = printMenu();
			if(userInput.equals("c") || userInput.equals("w") || userInput.equals("f") || userInput.equals("r") || userInput.equals("s") || userInput.equals("q")) {
				break;
			}
		}
		
		//call functions if that letter is chosen
		if(userInput.equals("c")) {
			getNumOfNonWSCharacters(text);
      printMenu();
		}
		
		else if(userInput.equals("w")) {
			getNumOfWords(text);
      printMenu();
		}
		
		else if(userInput.equals("f")) {
			findText("jeffrey",text);
      printMenu();
		}
		
		else if(userInput.equals("r")) {
			String newText = replaceExclamation(text);
			System.out.println(newText);
      printMenu();
		}
		
		else if(userInput.equals("s")) {
			String newText = shortenSpace(text);
			System.out.println(newText);
      printMenu();
		}
		
		else if(userInput.equals("q")) {
			quit();
		}
	}
	
	//method to prompt the user to enter valid text (not numbers)
	public static String sampleText() {
		
		String text = "";
		System.out.println("Please enter a block of text.");
		
		//check to make sure its text
		while(scanner.hasNext()) {
			if(scanner.hasNextDouble() == false && scanner.hasNextInt() == false) {
	    		text = scanner.nextLine();
	    		break;
	    	}
			else {
				System.out.println("Invalid response. Please enter a string.");
				scanner.next();
			}
		}
		
		//return the output
		System.out.println("You entered: " + text);
		System.out.println();
		return text;
	}
	
	//method to output to the screen the options a user can chose to go through after they type text
	public static String printMenu() {
		
		//options
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");
		System.out.println();
		
		String userInput = "a";
		System.out.println("Choose an option by typing a letter:");
		
		//check to make sure its a string
		while(scanner.hasNext()) {
			if(scanner.hasNextInt()) {
				System.out.println("Invalid input. Please type a letter.");
				scanner.next();
			}
			
			else {
				userInput = scanner.next();
				break;
			}
		}
		
		//return the output
		return userInput;
	}
	
	//method to print the number of letters in the user-entered text without including spaces
	public static int getNumOfNonWSCharacters(String userText) {
		
		int numCharacters = 0;
		for(int i = 0; i < userText.length(); i++) {
			System.out.print(userText.charAt(i));
			if(userText.charAt(i) == ' ') {
				numCharacters += 1;
			}
		}

		//return the output
		System.out.println();
		System.out.println(userText.length() - numCharacters);
		return numCharacters;
	}
	
	//method to count the number of words in the user-entered text
	public static int getNumOfWords(String userText) {
		
		int numWords = 1;
		for(int i = 0; i < userText.length(); i++) {
			if(userText.charAt(i) == ' ') {
				numWords += 1;
			}
		}
		
		//return the output
		System.out.println(numWords);
		return numWords;
	}
	
	//method to search for a specific word in the user-entered text
	public static int findText(String requestedWord, String userText) {
		
		int numInstances = 0;

		while(userText.indexOf(requestedWord) != -1) {
			userText.substring(userText.length() - 1);
			numInstances += 1;
				
			if(userText.indexOf(requestedWord) == -1) {
				break;
			}
		}
			
		//return the output
		System.out.println(numInstances);
		return numInstances;
	}
	
	//method to replace any exclamation points in the user-entered text with a period
	public static String replaceExclamation(String userText) {
		
		String newText = userText;
		
		for(int i = 0; i < userText.length(); i++) {
			if(userText.charAt(i) == '!') {
				newText = userText.replace("!",".");
			}
		}	
		
		//return the output
		return newText;
	}
	
	//method to replace any double spaces with single spaces in the user-entered text
	public static String shortenSpace(String userText) {
		
		String newText = userText;
		
		if(userText.indexOf("  ") != -1) {
			newText = userText.replace("  ", " ");
		}
		
		//return the output
		return newText;
	}
	
	//method to quit edited the text
	public static void quit() {
		System.out.println("Goodbye.");
		//return nothing (void function)
		return;
	}
}