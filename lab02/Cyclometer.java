//Jeffrey Barrett CSE 002 - 9/6/18

//Write a function that...
  //prints the number of minutes for each trip
  //prints the number of counts for each trip
  //prints the distance of each trip in miles
  //prints the distance for the two trips combined

//MPG records shows how fast a vechile can travel on a galon of fuel
public class Cyclometer {
    // main method required for every Java program
   	public static void main(String[] args) {
      
      // our input data. Document your variables by placing your
		  // comments after the slashes - State the purpose of each variable.

	   	int secsTrip1=480;  //how long trip 1 takes
      int secsTrip2=3220;  //how long trip 2 takes
		  int countsTrip1=1561;  //number of rotations for trip 1
		  int countsTrip2=9037; //number of rotations for trip 2
      
      // our intermediate variables and output data. Document!
      double wheelDiameter=27.0,  //size of wheel
  	  PI=3.14159, //number for the contast 'pi'
  	  feetPerMile=5280,  //number of feet in a mile
  	  inchesPerFoot=12,   //number of inches in a foot
  	  secondsPerMinute=60;  //number of seconds in a minute
	    double distanceTrip1, distanceTrip2,totalDistance;  //creating a variable for the distances of both trips
      
      System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); //print the calculated data
	    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); //print the calculated data
      
      //run the calculations; store the values. Document your
		  //calculation here. What are you calculating?
	    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	//Above gives distance in inches
      //For each count, a rotation of the wheel travels the diameter in inches times PI
      
      //calculates the total distance for the first trip
	    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
      
      //calculates the total distance for the second trip
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
      
      //calculates the distance for both trips together
	    totalDistance=distanceTrip1+distanceTrip2;
      
      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class








