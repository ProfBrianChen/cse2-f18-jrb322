//Jeffrey Barrett CSE 002, Lab 07 - Creating random sentences - Started 10/25/18

//import random and scanner methods
import java.util.Random;
import java.util.Scanner;

public class randomSentences {

	//initializing scanner and random
	static Random random = new Random();
	static Scanner scanner = new Scanner(System.in);
	
	//main method
	public static void main(String[] args) {
		
		//boolean to determine whether or not user wants to generate a new paragraph
		boolean newSentence = true;
		
		//while the user wants a new paragraph to be made, generate a new paragraph
		while(newSentence == true) {
			
			//call method
			createParagraph();
			
			//see if user wants another paragraph
			System.out.println("Would you like another paragraph to be generated. Type 1 for 'yes' and 2 for 'no'.");
			int userInput = 0;
			
			//making sure user input is valid and setting boolean to true or false based on user response
			while(scanner.hasNext()) {
				
				if(scanner.hasNextInt()) {
					
					userInput = scanner.nextInt();
					
					if(userInput == 1) {
						newSentence = true;
						break;
					}
					
					else if(userInput == 2) {
						newSentence = false;
						System.out.println("Okay. Bye.");
					}
					
					else {
						System.out.println("Invalid input. Please type 1 or 2.");
					}
				}
				
				else {
					System.out.println("Invalid input. Please type 1 or 2.");
					scanner.next();
				}
			}
		}
	}
	
	
	//create a method to generate an adjective
	public static String generateAdjectives() {
		
		int randomInt = random.nextInt(10);
		String adjective = " ";
		
		//list of possible adjectives to be chosen
		switch(randomInt) {
			case 0:
				adjective = "massive";
				break;
			case 1:
				adjective = "miniscule";
				break;
			case 2:
				adjective = "delightful";
				break;
			case 3:
				adjective = "fretful";
				break;
			case 4:
				adjective = "gentle";
				break;
			case 5:
				adjective = "tired";
				break;
			case 6:
				adjective = "bad";
				break;
			case 7:
				adjective = "elegant";
				break;
			case 8:
				adjective = "eager";
				break;
			case 9:
				adjective = "questionable";
				break;
		}
		return adjective;
	}
	
	//create a method to generate a noun
	public static String generateNounOne() {
		
		int randomInt = random.nextInt(10);
		String nounOne = " ";
		
		//list of possible nouns to be chosen
		switch(randomInt) {
			case 0:
				nounOne = "man";
				break;
			case 1:
				nounOne = "woman";
				break;
			case 2:
				nounOne = "snail";
				break;
			case 3:
				nounOne = "assailant";
				break;
			case 4:
				nounOne = "politician";
				break;
			case 5:
				nounOne = "beaver";
				break;
			case 6:
				nounOne = "bear";
				break;
			case 7:
				nounOne = "dog";
				break;
			case 8:
				nounOne = "cat";
				break;
			case 9:
				nounOne = "spy";
				break;
		}
		return nounOne;
	}
	
	//create a method to generate a verb
	public static String generateVerbs() {
		
		int randomInt = random.nextInt(10);
		String verb = " ";
		
		//list of possible verbs to be chosen
		switch(randomInt) {
			case 0:
				verb = "touched";
				break;
			case 1:
				verb = "liked";
				break;
			case 2:
				verb = "fought";
				break;
			case 3:
				verb = "called";
				break;
			case 4:
				verb = "ate";
				break;
			case 5:
				verb = "enjoyed";
				break;
			case 6:
				verb = "saw";
				break;
			case 7:
				verb = "became";
				break;
			case 8:
				verb = "tasted";
				break;
			case 9:
				verb = "attacked";
				break;
		}
		return verb;
	}
	
	//create a method to generate another noun
	public static String generateNounTwo() {
		
		int randomInt = random.nextInt(10);
		String nounTwo = " ";
		
		//list of possible nouns to be chosen
		switch(randomInt) {
			case 0:
				nounTwo = "skyline";
				break;
			case 1:
				nounTwo = "magazine";
				break;
			case 2:
				nounTwo = "package";
				break;
			case 3:
				nounTwo = "sandwich";
				break;
			case 4:
				nounTwo = "flower";
				break;
			case 5:
				nounTwo = "bee";
				break;
			case 6:
				nounTwo = "pillow";
				break;
			case 7:
				nounTwo = "mouse";
				break;
			case 8:
				nounTwo = "screen";
				break;
			case 9:
				nounTwo = "sand";
				break;
		}
		return nounTwo;
	}
	
	//create a method to generate a topic sentence and print it
	public static String createThesis() {
		String myAdjective = generateAdjectives();
		String firstNoun = generateNounOne();
		String verb = generateVerbs();
		String secondNoun = generateNounTwo();
		System.out.println("The " + myAdjective + " " + firstNoun + " " + verb + " the " + secondNoun + ".");
		
		return firstNoun;
	}
	
	//create a method to generate an action sentence using the subject from the first sentence
	public static String actionSentence(String subject) {
		String action = ("This " + subject + " " + generateVerbs() +  " the " + generateAdjectives() + " " + generateNounTwo() + ".");
		System.out.println(action);
		return action;
	}
	
	//create a method to generate a concluding sentence using the subject from the first sentence
	public static String concludingSentence(String subject) {
		String conclusion = ("That " + subject + " " + generateVerbs() + " their " + generateNounTwo() + ".");
		System.out.println(conclusion);
		return conclusion;
	}
	
	//create a method calling the functions to create a full paragraph
	//the action sentence could be anywhere from 1 - 10 sentences based on the random number generated
	public static void createParagraph() {
		
		int randomInt = random.nextInt(9) + 1;
		
		String subject = createThesis();
		for(int i = 0; i < randomInt; i++) {
			actionSentence(subject);
		}
		concludingSentence(subject);
	}
}