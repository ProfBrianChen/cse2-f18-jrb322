import java.util.Random;

public class CardGenerator {
  public static void main(String[] args) {
    
//generating a random number
    Random random = new Random();
    int cardNum = (int)random.nextInt(52) + 1;
    //int cardNum = 39;
    
//declaring variables to store card data
    String card = "";
    String suit = "";
    
//checking the suit of the cards + making sure cards only go from 2-10
    if(cardNum > 0 && cardNum <= 13) {
      suit = "Diamonds";
    }
    
    else if(cardNum > 13 && cardNum <=26) {
      suit = "Clubs";
      cardNum = cardNum - 13;
    }
    
    else if(cardNum > 26 && cardNum <= 39) {
      suit = "Hearts";
      cardNum = cardNum - 26;
    }
    
    else if(cardNum > 39 && cardNum <= 52) {
      suit = "Spades";
      cardNum = cardNum - 39;
    }
    
//checking for special cases for the card --> face cards
    if(cardNum == 1 || cardNum == 14 || cardNum == 27 || cardNum == 40) {
      card = "Ace";
    }
  
    else if(cardNum == 11 || cardNum == 24 || cardNum == 37 || cardNum == 50) {
      card = "Jack";
    }
  
    else if(cardNum == 12 || cardNum == 25 || cardNum == 38 || cardNum == 51) {
      card = "Queen";
    }
  
    else if(cardNum == 13 || cardNum == 26 || cardNum == 39 || cardNum == 52) {
      card = "King";
    }

    else {
      card = Integer.toString(cardNum);
    }
    
//printing final answer
    System.out.println("You picked the " + card + " of " + suit + "!");
  }
}