//importing scanner and random
import java.util.Scanner;
import java.util.Random;

public class Hw05 {
	
	public static void main(String[] args) {
	
	//variables for the 5 cards in a hand
	int card1 = 0;
	int card2 = 0;
	int card3 = 0;
	int card4 = 0;
	int card5 = 0;
	
	//types of hands (full-house, four of a king, three of a kind, two pair, one pair)
	int fk = 0;
	int tk = 0;
	int tp = 0;
	int op = 0;
	
	//variables for the probability of each hand
	double pfk = 0;
	double ptk = 0;
	double ptp = 0;
	double pop = 0;
	
	//variable for counter to end the for loop when counter reaches the value of the rounds the user wants to play
	int counter = 0;
	
	//initializing random and scanner
	Random rand = new Random();
	Scanner scanner = new Scanner(System.in);
	
	//getting the number of rounds the user wants to play
	System.out.println("How many rounds would you like to play?");
	
	//creating a variable for the user input
	int intRounds = 0;
	
	//if the user doesn't enter an integer, keep asking for a new input until they do
	while(scanner.hasNext()) {
		
		if(scanner.hasNextInt()) {
			intRounds = scanner.nextInt();
			break;
		}
		
		else {
			System.out.println("Invalid response. Please type an integer.");
			scanner.next();
		}
	}
	
	//while the counter is less than the number of rounds the user wants to play
	while(counter < intRounds) {
		
		//generate cards and make sure they are all distinct
		card1 = (int)rand.nextInt(52) + 1;
		
		card2 = (int)rand.nextInt(52) + 1;
		
		while(card2 == card1) {
			card2 = (int)rand.nextInt(52) + 1;
			break;
		}
		
		card3 = (int)rand.nextInt(52) + 1;
		
		while(card3 == card1 || card3 == card2) {
			card3 = (int)rand.nextInt(52) + 1;
			break;
		}
		
		card4 = (int)rand.nextInt(52) + 1;
		
		while(card4 == card1 || card4 == card2 || card4 == card3) {
			card4 = (int)rand.nextInt(52) + 1;
			break;
		}
		
		card5 = (int)rand.nextInt(52) + 1;
		
		while(card5 == card1 || card5 == card2 || card5 == card3 || card5 == card4) {
			card5 = (int)rand.nextInt(52) + 1;
			break;
		}
		
		//making cards the correct value for playing cards (not more than 13)
		card1 = card1 % 13;
		card2 = card2 % 13;
		card3 = card3 % 13;
		card4 = card4 % 13;
		card5 = card5 % 13;
		
		//checking for a full house + count it is a pair and three of a kind
		if(card1 == card2 && card3 == card4 && card3 == card5 || card1 == card3 && card2 == card4 && card2 == card5 || card1 == card4 && card2 == card3 && card2 == card5 || card1 == card5 && card2 == card3 && card2 == card4) {
			tk += 1;
			op += 1;
		}
			
		else if(card2 == card3 && card1 == card4 && card1 == card5 || card2 == card4 && card1 == card3 && card1 == card5 || card2 == card5 && card1 == card3 && card1 == card4) {
			tk += 1;
			op += 1;
		}
			
		else if(card3 == card4 && card1 == card2 && card1 == card5 || card3 == card5 && card1 == card2 && card1 == card3) {
			tk += 1;
			op += 1;
		}
			
		else if(card4 == card5 && card1 == card2 && card1 == card3) {
			tk += 1;
			op += 1;
		}
		
		//checking for four of a king
		else if(card1 == card2 && card1 == card3 && card1 == card4 || card1 == card3 && card1 == card4 && card1 == card5 || card1 == card2 && card1 == card3 && card1 == card5 || card1 == card2 && card1 == card4 && card1 == card5) {
			fk += 1;
		}
		
		else if(card2 == card3 && card2 == card4 && card2 == card5) {
			fk += 1;
		}
		
		else if(card3 == card4 && card3 == card5 && card3 == card1) {
			fk += 1;
		}
		
		else if(card4 == card5 && card4 == card1 && card4 == card3) {
			fk += 1;
		}
		
		else if (card5 == card1 && card5 == card2 && card5 == card3) {
			fk += 1;
		}
		
		//checking for three of a kind
		else if(card1 == card2 && card1 == card3 || card1 == card2 && card1 == card4 || card1 == card2 && card1 == card5 || card1 == card3 && card1 == card4 || card1 == card3 && card1 == card5 || card1 == card4 && card1 == card5) {
			tk += 1;
		}
		
		else if(card2 == card1 && card2 == card4 || card2 == card1 && card2 == card5 || card2 == card3 && card2 == card4 || card2 == card3 && card2 == card5 || card2 == card4 && card2 == card5) {
			tk += 1;
		}
		
		else if(card3 == card4 && card3 == card5 || card3 == card2 && card3 == card4 || card3 == card1 && card3 == card4 || card3 == card1 && card3 == card5 || card3 == card2 && card3 == card5) {
			tk += 1;
		}
		
		else if(card4 == card3 && card4 == card5) {
			tk += 1;
		}
		
		//checking for a two-pair
		else if(card1 == card2 && card3 == card4 || card1 == card2 && card3 == card5 || card1 == card2 && card4 == card5 || card1 == card3 && card2 == card4 || card1 == card3 && card2 == card5 || card1 == card3 && card4 == card5 || card1 == card4 && card2 == card3 || card1 == card4 && card2 == card5 || card1 == card4 && card3 == card5 || card1 == card5 && card2 == card3 || card1 == card5 && card2 == card4 || card1 == card5 && card3 == card4) {
			tp += 1;
		}
		
		//checking for a pair
		else if(card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5) {
			op += 1;
		}
		
		else if(card2 == card3 || card2 == card4 || card2 == card5) {
			op += 1;
		}
		
		else if(card3 == card4 || card3 == card5) {
			op += 1;
		}
		
		else if(card4 == card5) {
			op += 1;
		}
		
		counter += 1;
		
	}
	
	//convert int rounds to a double so we can get a decimal when dividing by the variable
	double dbRounds = (double)intRounds;
	
	//final print statements to describe the final results of the program
	
//	System.out.println("four of a kind " + fk);
//	System.out.println("three of a kind " + tk);
//	System.out.println("two-pair " + tp);
//	System.out.println("one pair " + op);
//	System.out.println();
	
	System.out.println("The user entered " + counter + " rounds");
	
	//getting the probability for each hand
	pfk = fk / dbRounds;
	ptk = tk / dbRounds;
	ptp = tp / dbRounds;
	pop = op / dbRounds;
	
	//final print statements to describe the final results of the program
	System.out.printf("\n" + "The probability for getting four of a kind, rounded to 3 decimals places, is " + "%.3f",pfk);
	System.out.printf("\n" + "The probability for getting three of a kind, rounded to 3 decimals places, is " + "%.3f",ptk);
	System.out.printf("\n" + "The probability for getting a two-pair, rounded to 3 decimals places, is " + "%.3f",ptp);
	System.out.printf("\n" + "The probability for getting one pair, rounded to 3 decimals places, is " + "%.3f",pop);
	}
}