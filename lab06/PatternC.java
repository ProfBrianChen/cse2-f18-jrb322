//Jeffrey Barrett - Lab 06 - 10/11/18 - Patterns using for loops
import java.util.Scanner;

public class PatternC {
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		int input = 0;
		
		//ask the user for an input between 1-10
		System.out.println("Please type an integer between 1 - 10");
		//check to see if its in range and if its an int
		while(scanner.hasNext()) {
			
			if(scanner.hasNextInt()) {
				input = scanner.nextInt();
				
				if(input > 0 && input < 10) {
					System.out.println(input + " lines will be printed");
					break;
				}
				
				else {
					System.out.println("Not a valid response. Please type an integer between 1 - 10");
					scanner.next();
				}
			}
			
			else {
				System.out.println("Not a valid response. Please type an integer between 1 - 10");
				scanner.next();
			}
		}
		
		//nested for loop to display pattern c
		for(int i = 1; i <= input; i++) {
			for(int k = input; k >= i; k--) {
				System.out.print(" ");
			}
			for(int j = i; j >= 1; j--) {
				System.out.print(j);
			}
			System.out.println();
		}
	}
}