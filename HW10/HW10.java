//Jeffrey Barrett - Homework 10 - Tic-Tac-Toe - Due Tuesday, December 4th

//importing necessary stuff
import java.util.Scanner;
import java.util.Arrays;
import java.util.*;

public class HW10 {
	
	//variables to get user input
	static int rowp1 = 0;
	static int colp1 = 0;
	static int rowp2 = 0;
	static int colp2 = 0;
	
	//main method
	public static void main(String[] args) {
		
		//creating scanner and multidimensional array
		Scanner scanner = new Scanner(System.in);
		String[][] board = {{"1","2","3"},{"4","5","6"},{"7","8","9"}};
		//call function to display the board
		displayBoard();
		
		//while the game is not over, ask the user to enter a row and column and make sure their answer is works
		while(determineWinner(board) == false) {
			//ask user to enter a row + check answer
			System.out.println("Player 1, what row would you like to put an 'X' at? Please type 0, 1, or 2.");
			while(scanner.hasNext()) {
				if(scanner.hasNextInt()) {
					rowp1 = scanner.nextInt();
					if(rowp1 > 2 || rowp1 < 0) {
						System.out.println("Invalid response. Please type an integer between 0-2.");
					}
					else {
						break;
					}
				}
				else {
					System.out.println("Invalid response. Please type an integer.");
					scanner.next();
				}
			}
			//ask user to enter a column + check answer
			System.out.println("Player 1, what column would you like to put an 'X' at? Please type 0, 1, or 2.");
			while(scanner.hasNext()) {
				if(scanner.hasNextInt()) {
					colp1 = scanner.nextInt();
					if(colp1 > 2 || colp1 < 0) {
						System.out.println("Invalid response. Please type an integer between 0-2.");
					}
					else {
						break;
					}
				}
				else {
					System.out.println("Invalid response. Please type an integer.");
					scanner.next();
				}
			}
			//call a function to show the new board after user 1 has input their data and check to see if they won
			displayNewBoard(board);
			//ask user to enter a row + check answer
			System.out.println("Player 2, what row would you like to put an 'O' at? Please type 0, 1, or 2.");
			while(scanner.hasNext()) {
				if(scanner.hasNextInt()) {
					rowp2 = scanner.nextInt();
					if(rowp2 > 2 || rowp2 < 0) {
						System.out.println("Invalid response. Please type an integer between 0-2.");
					}
					else {
						break;
					}
				}
				else {
					System.out.println("Invalid response. Please type an integer.");
					scanner.next();
				}
			}
			//ask user to enter a column + check answer
			System.out.println("Player 2, what column would you like to put an 'X' at? Please type 0, 1, or 2.");
			while(scanner.hasNext()) {
				if(scanner.hasNextInt()) {
					colp2 = scanner.nextInt();
					if(colp2 > 2 || colp2 < 0) {
						System.out.println("Invalid response. Please type an integer between 0-2.");
					}
					else {
						break;
					}
				}
				else {
					System.out.println("Invalid response. Please type an integer.");
					scanner.next();
				}
			}
			//call a function to show the new board after user 2 has input their data and check to see if they won
			displayNewBoard(board);
		}
	}
	
	//function to display the original board
	public static void displayBoard() {
		System.out.println("Tic-Tac-Toe Board:");
		String[][] board = {{"1","2","3"},{"4","5","6"},{"7","8","9"}};
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board.length; j++) {
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	//function to display the new board after the users input rows and columns
	public static String[][] displayNewBoard(String[][] board) {
		String[][] newBoard = new String[board.length][board.length];
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board.length; j++) {
				board[rowp1][colp1] = "X";
				board[rowp2][colp2] = "0";
				System.out.print(board[i][j] + " ");
				if(board[i][j].contains("X") && board[i][j].contains("O") && determineWinner(board) == false) {
					System.out.println("Draw");
				}
			}
			System.out.println();
		}
		return newBoard;
	}
	
	//function to determine when / if a player wins
	public static boolean determineWinner(String[][] board) {
		//checking vertically for "X" winner
		if(board[0][0].contains("X") && board[0][1].contains("X") && board[0][2].contains("X")) {
			System.out.println("Player 1 Wins!");
			return true;
		}
		else if(board[1][0].contains("X") && board[1][1].contains("X") && board[1][2].contains("X")) {
			System.out.println("Player 1 Wins!");
			return true;
		}
		else if(board[2][0].contains("X") && board[2][1].contains("X") && board[2][2].contains("X")) {
			System.out.println("Player 1 Wins!");
			return true;
		}
		//checking horizontally for "X" winner
		else if(board[0][0].contains("X") && board[1][0].contains("X") && board[2][0].contains("X")) {
			System.out.println("Player 1 Wins!");
			return true;
		}
		else if(board[0][1].contains("X") && board[1][1].contains("X") && board[2][1].contains("X")) {
			System.out.println("Player 1 Wins!");
			return true;
		}
		else if(board[0][2].contains("X") && board[2][1].contains("X") && board[2][2].contains("X")) {
			System.out.println("Player 1 Wins!");
			return true;
		}
		//checking diagonally for "X" winner
		else if(board[0][0].contains("X") && board[1][1].contains("X") && board[2][2].contains("X")) {
			System.out.println("Player 1 Wins!");
			return true;
		}
		else if(board[2][0].contains("X") && board[1][1].contains("X") && board[0][2].contains("X")) {
			System.out.println("Player 1 Wins!");
			return true;
		}
		
		//checking vertically for "O" winner
		if(board[0][0].contains("O") && board[0][1].contains("O") && board[0][2].contains("O")) {
			System.out.println("Player 2 Wins!");
			return true;
		}
		else if(board[1][0].contains("O") && board[1][1].contains("O") && board[1][2].contains("O")) {
			System.out.println("Player 2 Wins!");
			return true;
		}
		else if(board[2][0].contains("O") && board[2][1].contains("O") && board[2][2].contains("O")) {
			System.out.println("Player 2 Wins!");
			return true;
		}
		//checking horizontally for "O" winner
		else if(board[0][0].contains("O") && board[1][0].contains("O") && board[2][0].contains("O")) {
			System.out.println("Player 2 Wins!");
			return true;
		}
		else if(board[0][1].contains("O") && board[1][1].contains("O") && board[2][1].contains("O")) {
			System.out.println("Player 2 Wins!");
			return true;
		}
		else if(board[0][2].contains("O") && board[2][1].contains("O") && board[2][2].contains("O")) {
			System.out.println("Player 2 Wins!");
			return true;
		}
		//checking diagonally for "O" winner
		else if(board[0][0].contains("O") && board[1][1].contains("O") && board[2][2].contains("O")) {
			System.out.println("Player 2 Wins!");
			return true;
		}
		else if(board[2][0].contains("O") && board[1][1].contains("O") && board[0][2].contains("O")) {
			System.out.println("Player 2 Wins!");
			return true;
		}
		//if none of these are true, return false
		return false;
	}
}