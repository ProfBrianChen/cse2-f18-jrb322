//Jeffrey Barrett CrapsIf code -- 9/21/18

//importing scanner and random function
import java.util.Scanner;
import java.util.Random;

public class CrapsIf {
  public static void main(String[] args) {
    
//declaring scanner and random variables
    Scanner scanner = new Scanner(System.in);
    Random random = new Random();
    
//asking user if they want to chose numbers or have them selected
    System.out.println("Would you like to pick 2 numbers from the dice or have them randomly selected?");
    System.out.println("Type 1 to choose them or 2 to have them selected.");
    
//declaring userSelect int
    int userSelect = scanner.nextInt();
    
//if user wants to choose numbers, ask them for the numbers and store that value in die1 and die2
    if(userSelect == 1) {
      System.out.println("What value would you like to assign to the first die? Please choose a number between 1 and 6.");
      int die1 = scanner.nextInt();
      System.out.println("You chose " + die1 + ". Now please pick a number for the second die, again between 1 and 6.");
      int die2 = scanner.nextInt();
      System.out.println("The second die has a value of " + die2);
      
//going through all possibilities of sets of numbers in craps
      if(die1 == 1 && die2 == 1) {
        System.out.println("Snake eyes");
      }
      
      else if((die1 == 1 || die2 == 1) && (die1 == 2 || die2 == 2)) {
        System.out.println("Ace Duece");
      }
      
      else if(die1 == 2 && die2 == 2) {
        System.out.println("Hard Four");
      }
      
      else if((die1 == 1 || die2 == 1) && (die1 == 3 || die2 == 3)) {
        System.out.println("Easy Four");
      }
      
      else if((die1 == 2 || die2 == 2) && (die1 == 3 || die2 == 3)) {
        System.out.println("Fever Five");
      }
      
      else if(die1 == 3 && die2 == 3) {
        System.out.println("Hard Six");
      }
      
      else if((die1 == 1 || die2 == 1) && (die1 == 4 || die2 == 4)) {
        System.out.println("Fever Five");
      }
      
      else if((die1 == 2 || die2 == 2) && (die1 == 4 || die2 == 4)) {
        System.out.println("Easy Six");
      }
      
      else if((die1 == 3 || die2 == 3) && (die1 == 4 || die2 == 4)) {
        System.out.println("Seven Out");
      }
      
      else if(die1 == 4 && die2 == 4) {
        System.out.println("Hard Eight");
      }
      
      else if((die1 == 1 || die2 == 1) && (die1 == 5 || die2 == 5)) {
        System.out.println("Easy Six");
      }
      
      else if((die1 == 2 || die2 == 2) && (die1 == 5 || die2 == 5)) {
        System.out.println("Seven Out");
      }
      
      else if((die1 == 3 || die2 == 3) && (die1 == 5 || die2 == 5)) {
        System.out.println("Easy Eight");
      }
      
      else if((die1 == 4 || die2 == 4) && (die1 == 5 || die2 == 5)) {
        System.out.println("Nine");
      }
      
      else if(die1 == 5 && die2 == 5) {
        System.out.println("Hard Ten");
      }
      
      else if((die1 == 1 || die2 == 1) && (die1 == 6 || die2 == 6)) {
        System.out.println("Seven Out");
      }
      
      else if((die1 == 2 || die2 == 2) && (die1 == 6 || die2 == 6)) {
        System.out.println("Easy Eight");
      }
      
      else if((die1 == 3 || die2 == 3) && (die1 == 6 || die2 == 6)) {
        System.out.println("Nine");
      }
      
      else if((die1 == 4 || die2 == 4) && (die1 == 6 || die2 == 6)) {
        System.out.println("Easy Ten");
      }
      
      else if((die1 == 5 || die2 == 5) && (die1 == 6 || die2 == 6)) {
        System.out.println("Yo-leven");
      }
      
      else if(die1 == 6 && die2 == 6) {
        System.out.println("Boxcars");
      }
    }
    
//if the user wants the numbers picked for them, generate 2 random numbers
    else if(userSelect == 2) {
      int die1 = (int)random.nextInt(6) + 1;
      int die2 = (int)random.nextInt(6) +1;
      System.out.println("The two random numbers have been generated. The first die is a " + die1 + " and the second die is a " + die2);
      
//going through all possibilities of sets of numbers in craps
      if(die1 == 1 && die2 == 1) {
        System.out.println("Snake eyes");
      }
      
      else if((die1 == 1 || die2 == 1) && (die1 == 2 || die2 == 2)) {
        System.out.println("Ace Duece");
      }
      
      else if(die1 == 2 && die2 == 2) {
        System.out.println("Hard Four");
      }
      
      else if((die1 == 1 || die2 == 1) && (die1 == 3 || die2 == 3)) {
        System.out.println("Easy Four");
      }
      
      else if((die1 == 2 || die2 == 2) && (die1 == 3 || die2 == 3)) {
        System.out.println("Fever Five");
      }
      
      else if(die1 == 3 && die2 == 3) {
        System.out.println("Hard Six");
      }
      
      else if((die1 == 1 || die2 == 1) && (die1 == 4 || die2 == 4)) {
        System.out.println("Fever Five");
      }
      
      else if((die1 == 2 || die2 == 2) && (die1 == 4 || die2 == 4)) {
        System.out.println("Easy Six");
      }
      
      else if((die1 == 3 || die2 == 3) && (die1 == 4 || die2 == 4)) {
        System.out.println("Seven Out");
      }
      
      else if(die1 == 4 && die2 == 4) {
        System.out.println("Hard Eight");
      }
      
      else if((die1 == 1 || die2 == 1) && (die1 == 5 || die2 == 5)) {
        System.out.println("Easy Six");
      }
      
      else if((die1 == 2 || die2 == 2) && (die1 == 5 || die2 == 5)) {
        System.out.println("Seven Out");
      }
      
      else if((die1 == 3 || die2 == 3) && (die1 == 5 || die2 == 5)) {
        System.out.println("Easy Eight");
      }
      
      else if((die1 == 4 || die2 == 4) && (die1 == 5 || die2 == 5)) {
        System.out.println("Nine");
      }
      
      else if(die1 == 5 && die2 == 5) {
        System.out.println("Hard Ten");
      }
      
      else if((die1 == 1 || die2 == 1) && (die1 == 6 || die2 == 6)) {
        System.out.println("Seven Out");
      }
      
      else if((die1 == 2 || die2 == 2) && (die1 == 6 || die2 == 6)) {
        System.out.println("Easy Eight");
      }
      
      else if((die1 == 3 || die2 == 3) && (die1 == 6 || die2 == 6)) {
        System.out.println("Nine");
      }
      
      else if((die1 == 4 || die2 == 4) && (die1 == 6 || die2 == 6)) {
        System.out.println("Easy Ten");
      }
      
      else if((die1 == 5 || die2 == 5) && (die1 == 6 || die2 == 6)) {
        System.out.println("Yo-leven");
      }
      
      else if(die1 == 6 && die2 == 6) {
        System.out.println("Boxcars");
      }
    }
    
//if the user choses a number that isnt 1 or 2, tell them to pick a number again
    else {
      System.out.println("That answer is not valid. Please type 1 or 2.");
      userSelect = scanner.nextInt();
    }
  }
}