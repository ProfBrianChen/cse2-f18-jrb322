//Jeffrey Barrett 10-8-18 --> Lab 08 - Using 2 arrays to count he number of occurences of a number in an array

//import random functions
import java.util.Random;

public class arrays {

	public static void main(String[] args) {
		
		//initialize random function
		Random random = new Random();
		
		//create 2 int arrays of size 100
		int[] numArray = new int[100];
		int[] countArray = new int[100];
		
		//make temp value to store data
		int tempValue = 0;
		//for loop to loop through all index's of the array, and count the number of occurences of each number
		for(int i = 0; i < numArray.length; i++) {
			numArray[i] = random.nextInt(100);
			tempValue = numArray[i];
			countArray[tempValue] += 1;
		}
		
		//loop through numArray and print the number of occurences of each number
		for (int i = 0; i < numArray.length; i++){
			if(countArray[numArray[i]] < 2) {
				System.out.println(numArray[i] + " occurs " + countArray[numArray[i]] + " time");
			}
			else {
				System.out.println(numArray[i] + " occurs " + countArray[numArray[i]] + " times");
			}
		}
	}
}