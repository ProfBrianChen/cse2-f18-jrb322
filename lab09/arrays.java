//Jeffrey Barrett - Lab 09 - Passing Arrays Inside Methods - Due Next Lab

//import arrays
import java.util.Arrays;

public class arrays {

	public static void main(String[] args) {
		//create 3 arrays
		int[] array0 = {1,2,3,4,5,6,7,8,9};
		int[] array1 = copy(array0);
		int[] array2 = copy(array0);
		
		//call functions
		inverter(array0);
		print(array0);
		inverter2(array1);
		print(array1);
		int[] array3 = inverter2(array2);
		print(array3);
		inverter2(array0);
	}
	
	//function to copy the input array into a new array
	public static int[] copy(int[] array) {
		int[] newArray = new int[array.length];
		for(int i = 0; i < newArray.length; i++) {
			newArray[i] = array[i];
		}
		//return the new array
		return newArray;
	}
	
	//function to invert the array - no return statement
	public static void inverter(int[] array) {
		for(int i = 0; i < array.length/2; i++) {
			int temp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = temp;
		}
	}
	
	//function to copy an array and invert one of them
	public static int[] inverter2(int[] array) {
		int[] newArray = copy(array);
		for(int i = 0; i < array.length/2; i++) {
			int temp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = temp;
		}
		//return the array
		return newArray;
	}
	
	public static void print(int[] array) {
		System.out.println(Arrays.toString(array));
	}
}