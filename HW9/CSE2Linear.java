//Jeffrey Barrett - Hw09 - using binary and linear search to find a number - due 11/27/18

//import random, scanner, and arrays
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class CSE2Linear {
	
	//initialize random and allow random to be used in all methods
	static Random random = new Random();
	
	public static void main(String[] args) {
		
		//initialize scanner, create an array, and prompt user to enter numbers
		Scanner scanner = new Scanner(System.in);
		int[] grades = new int[15];
		System.out.println("Enter 15 ascending ints for final grades in CSE2:");
		
		//get user input and make sure it is valid input
		for(int i = 0; i < grades.length; i++) {
			while(scanner.hasNext()) {
				if(scanner.hasNextInt()) {
					grades[i] = scanner.nextInt();
					if(grades[i] < 0 || grades[i] > 100) {
						System.out.println("Enter an integer between 0 and 100.");
						scanner.next();
					}
					else {
						break;
					}
				}
				else {
					System.out.println("Enter an integer");
					scanner.next();
				}
			}
		}
		//make input appear in array form
		System.out.println(Arrays.toString(grades));
		
		//prompt user for a number
		System.out.println("What grade would you like to be searched for?");
		int searchNum = scanner.nextInt();
		
		//call functions
		binarySearch(searchNum, grades);
		scramble(grades);
		linearSearch(searchNum,grades);
	}
	
	//function to search the grades linearly
	public static int linearSearch(int targetNum, int[] myGrades) {
		int count = 0;
		
		//check for the number and increase the count if the number is not found
		for(int i = 0; i < myGrades.length; i++) {
			if(targetNum != myGrades[i]) {
				count += 1;
			}
			//in the number is found, return the grade
			else if(targetNum == myGrades[i]) {
				count += 1;
				System.out.println(count);
				return myGrades[i];
			}
		}
		//return -1 if not found
		System.out.println(count);
		return -1;
	}
	
	//function to search for a number by cutting the array in half until it is found
	public static int binarySearch(int targetNum, int[] myGrades) {
		int low = 0;
		int high = myGrades.length-1;
		int count = 0;
		int mid = (low + high) /2;
		
		//code to cut the code in half to look at the array in order to find the target number
		while(high >= low) {
			mid = (low + high) /2;
			if(targetNum < myGrades[mid]) {
				count += 1;
				high = mid - 1;
			}
			//if its found, return the number of times it takes to find it
			else if(targetNum == myGrades[mid]) {
				count += 1;
				System.out.println(count);
				return mid;
			}
			else if(targetNum > myGrades[mid]) {
				low = mid + 1;
				count += 1;
			}
		}
		//if its not found, return -1
		System.out.println(count);
		return -1;
	}
	
	//function to randomly scramble the array
	public static int[] scramble(int[] gradeArray) {
		int temp = 0;
		
		//use temp value to scramble the array
		for(int i = 0; i < gradeArray.length; i++) {
			int rand = random.nextInt(gradeArray.length);
			temp = gradeArray[rand];
			gradeArray[rand] = gradeArray[0];
			gradeArray[0] = temp;
		}

		//print the array and return it
		System.out.println(Arrays.toString(gradeArray));
		return gradeArray;
	}
}