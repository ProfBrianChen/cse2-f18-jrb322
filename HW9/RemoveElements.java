//Jeffrey Barrett - Hw09 - removing elements from an array - due 11/27/18

//importing scanner and random
import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
	
	//initialize random and allow random to be used in all methods
	static Random random = new Random();
	
	public static void main(String [] arg){
		
		//your code - declaring and initializing variables / arrays
		Scanner scan=new Scanner(System.in);
		int num[]=new int[10];
		int newArray1[];
		int newArray2[];
		int index,target;
		String answer="";
		//do-while loop to print text for user, allow user input, and call functions
		do{
			System.out.print("Random input 10 ints [0-9]");
			num = randomInput();
			String out = "The original array is:";
			out += listArray(num);
			System.out.println(out);
 
			//do-while loop to print text for user, allow user input, and call functions
			System.out.print("Enter the index ");
			index = scan.nextInt();
			newArray1 = delete(num,index);
			String out1="The output array is ";
			out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
			System.out.println(out1);
 
			//do-while loop to print text for user, allow user input, and call functions
			System.out.print("Enter the target value ");
			target = scan.nextInt();
			newArray2 = remove(num,target);
			String out2="The output array is ";
			out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
			System.out.println(out2);
  	 
			//do-while loop to print text for user, allow user input, and call functions
			System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
			answer=scan.next();
		}
		//while part of do-while loop
		while(answer.equals("Y") || answer.equals("y"));
	}
 
	//function to print the array
	public static String listArray(int num[]){
		String out="{";
		for(int j=0;j<num.length;j++){
			if(j>0){
				out+=", ";
			}
			out+=num[j];
		}
		out+="} ";
		return out;
	}
  
	//function to populate an array with random integers 0-9
	public static int[] randomInput() {
		int[] myArray = new int[10];
		
		//give array random numbers
		for(int i = 0; i < myArray.length; i++) {
			int rand = random.nextInt(10);
			myArray[i] = rand;
		}
		//return new array
		return myArray;
	}
	
	//function to delete the requested index of the array
	public static int[] delete(int[] list, int pos) {
		int[] newArray = new int[list.length - 1];
		
		//swap array values and make the array 1 less index to 'cut it'
		list[pos] = list[list.length - 1];
		for(int i = 0; i < newArray.length; i ++) {
			newArray[i] = list[i];
		}
		//return new array
		return newArray;
	}
	
	//function to remove all indexes of an array that have the target number
	public static int[] remove(int[] list, int target) {
		//count the number of times the target number is in the array
		int count = 0;
		for(int i = 0; i < list.length; i++) {
			if(list[i] == target) {
				count += 1;
			}
		}
		//create a new array with a size of the original array minus the number of times the target number appears
		int[] newArray = new int[list.length - count];
		
		//swap array values / cut the size of the array
		for(int i = 0; i < newArray.length; i++) {
			newArray[i] = list[i];
			if(newArray[i] == target) {
				newArray[i] = newArray[newArray.length - count];
			}
		}
		list[target] = list[list.length - 1];
		//return the new array
		return newArray;
	}
}