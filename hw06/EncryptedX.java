//Jeffrey Barrett - hw 06 - 10/19/18 - Encrypted X - display an encrypted x to the screen based off the users input

import java.util.Scanner;

public class EncryptedX {

	public static void main(String[] args) {
		
		//make a scanner
		Scanner scanner = new Scanner(System.in);
		
		//ask the user for the character they would like to be displayed
		System.out.println("What character would you like to be displayed?");
    //create variable with a temp value
		char result = 'a';
		
    //repeat prompt until the user gives a valid response
		while(scanner.hasNext()) {
			
			if(scanner.hasNextInt()) {
				System.out.println("Not a valid response. Please type a character.");
				scanner.next();
			}
			
			else {
				String temp = scanner.next();
				result = temp.charAt(0);
				break;
			}
		}
		
		//ask the user for the number of generations they want this to run for
		System.out.println("How many generations would you like this to run for? (no more than 100)");
    //create variable with a temp value
		int numGen = 0;;
		
    //repeat prompt until the user gives a valid response
		while(scanner.hasNext()) {
			
			if(scanner.hasNextInt()) {
				numGen = scanner.nextInt();
				
				if(numGen <= 100) {
					break;
				}
				
				else {
					System.out.println("Not a valid response. Please type an integer of 100 or less.");
				}
			}
			
			else {
				System.out.println("Not a valid response. Please type an integer.");
				scanner.next();
			}
		}
		
		//display fractal pattern
		for(int i = 0; i < numGen; i++) {
			for(int j = 0; j < numGen; j++) {
				
				//statements to only display the pattern when appropriate so that it makes an x
				if(i == j) {
					System.out.print(" ");
				}
				
				else if (i + j == numGen-1){
					System.out.print(" ");
				}
				
				else {
					System.out.print(result);
				}
			}
			System.out.println();
		}
	}
}
