public class Arithmetic {
  
  public static void main(String[] args) {
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;

// Total cost of each kind of item (i.e. total cost of pants, etc)
// Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
// Total cost of purchases (before tax)
// Total sales tax
// Total paid for this transaction, including sales tax. 
    
    double totalCostOfPants;//total cost of pants
    double totalCostOfShirts;//total cost of shirts
    double totalCostOfBelts;//total cost of belts
    
    double salesTaxPants;//sales tax on pants
    double salesTaxShirts;//sales tax on shirts
    double salesTaxBelts;//sales tax on belts
    
    double costBeforeTax;//total price without tax added
    
    double totalSalesTax;//total price of tax
    
    double finalPrice;//total price before tax + total sales tax
    
    //calculations needed including the variables above - finding the cost of these items based on the number of them and their price
    totalCostOfPants = numPants * pantsPrice;
    totalCostOfShirts = numShirts * shirtPrice;
    totalCostOfBelts = numBelts * beltCost;
    
    //printing out the calculations with a message before
    System.out.println("The total price for buying these pants is $" + totalCostOfPants);
    System.out.println("The total price for buying these shirts is $" + totalCostOfShirts);
    System.out.println("The total price for buying these belts is $" + totalCostOfBelts);
    
    //finding the sales tax on the following 3 sets of items
    salesTaxPants = totalCostOfPants * paSalesTax;
    salesTaxShirts = totalCostOfShirts * paSalesTax;
    salesTaxBelts = totalCostOfBelts * paSalesTax;
    
    //converting numbers with a lot of decimals to an int --> for pants
    salesTaxPants = salesTaxPants * 100;
    salesTaxPants = (int) salesTaxPants;
    salesTaxPants = salesTaxPants / 100;
    
    //converting number with a lot of decimals to an int --> for shirts
    salesTaxShirts = salesTaxShirts * 100;
    salesTaxShirts = (int) salesTaxShirts;
    salesTaxShirts = salesTaxShirts / 100;
    
    //converting number with a lot of decimals to an int --> for belts
    salesTaxBelts = salesTaxBelts * 100;
    salesTaxBelts = (int) salesTaxBelts;
    salesTaxBelts = salesTaxBelts / 100;
    
    //printing out the calculated number for sales tax on the following 3 items
    System.out.println("The sales tax on the pants is $" + salesTaxPants);
    System.out.println("The sales tax on the shirts is $" + salesTaxShirts);
    System.out.println("The sales tax on the belts is $" + salesTaxBelts);
    
    //printing out the total cost of these items before adding on tax
    costBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    System.out.println("The cost of these items before tax is $" + costBeforeTax);
    
    //printing out the total tax on the 3 sets of items added together
    totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts;
    System.out.println("The total sales tax on these items is $" + totalSalesTax);
    
    //printing out the final price of all the items, including the sales tax
    finalPrice = costBeforeTax + totalSalesTax;
    System.out.println("The total price, including sales tax, for these items is $" + finalPrice);
  }
}