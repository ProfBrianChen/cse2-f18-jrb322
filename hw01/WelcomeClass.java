public class WelcomeClass {
  public static void main(String args[]) {
    System.out.println("-----------");
    System.out.println("| Welcome |");
    System.out.println("-----------");
    System.out.println("^ ^ ^ ^ ^ ^ ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--R--B--3--2--2->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("v  v  v  v  v  v");
    System.out.println("I am from Westchester, New York and I played football and lacrosse in high school");
  }
}