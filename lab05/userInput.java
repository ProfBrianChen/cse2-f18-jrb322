//importing scanner
import java.util.Scanner;

public class userInput {

	public static void main(String[] args) {
	    
		  //creating scanner
	    Scanner scanner = new Scanner(System.in);

	    //asking user to input data + creating variable to store that data later
	    System.out.println("Please answer the following questions all related to your favorite course");
	    System.out.println("What is the course number?");
	    int courseNum = 0;
	    
	    //continue to ask the user for input until the user responds properly
	    while(scanner.hasNext()) {
	      
	      //if the user enters the correct input, set the variable equal to their input and end the for loop
	      if(scanner.hasNextInt()) {
	    	courseNum = scanner.nextInt();
	        break;
	      }
	      
	      //otherwise, tell the user to re-input a valid answer and look for the next input
	      else {
		    System.out.println("Invalid response. Please type an integer.");
		    scanner.next();
	      }
	    }
	    
	    //asking user to input data + creating variable to store that data later
	    System.out.println("What is the department name?");
	    String deptName = "";
	    
	    //continue to ask the user for input until the user responds properly
	    while(scanner.hasNext()) {
	      
	      //if the user enters the correct input, set the variable equal to their input and end the for loop
	      if(scanner.hasNextDouble() == false && scanner.hasNextInt() == false) {
	    	deptName = scanner.next();
	        break;
	      }
	      
	      //otherwise, tell the user to re-input a valid answer and look for the next input
	      else {
	    	System.out.println("Not a valid response. Please print the name of the department.");
	    	scanner.next();
	      }
	    }
	    
	    //asking user to input data + creating variable to store that data later
	    System.out.println("How many times does this course meet per week?");
	    int numMeetings = 0;
	    
	    //continue to ask the user for input until the user responds properly
	    while(scanner.hasNext()) {
		   
	    	//if the user enters the correct input, set the variable equal to their input and end the for loop
	    	if(scanner.hasNextInt()) {
	    		numMeetings = scanner.nextInt();
	    		break;
	    	}
		    
	    	//otherwise, tell the user to re-input a valid answer and look for the next input
	    	else {
			  System.out.println("Invalid response. Please type an integer.");
			  scanner.next();
	    	}
	    }
	    
	    //asking user to input data + creating variable to store that data later
	    System.out.println("How many hours does the course meet for?");
	    int hours = 0;
	    
	    //continue to ask the user for input until the user responds properly
	    while(scanner.hasNext()) {
	    	
	    	//if the user enters the correct input, set the variable equal to their input and end the for loop
	    	if(scanner.hasNextInt()) {
	    		hours = scanner.nextInt();
	    		break;
	    	}
	    	
	    	//otherwise, tell the user to re-input a valid answer and look for the next input
	    	else {
	    		System.out.println("Invalid response. Please type an integer.");
	    		scanner.next();
	    	}
	    }
	    
	    //asking user to input data + creating variable to store that data later
	    System.out.println("How many minutes does the course meet for?");
	    int minutes = 0;
	    
	    //continue to ask the user for input until the user responds properly
	    while(scanner.hasNext()) {
	    	
	    	//if the user enters the correct input, set the variable equal to their input and end the for loop
	    	if(scanner.hasNextInt()) {
	    		minutes = scanner.nextInt();
	    		break;
	    	}
	    	
	    	//otherwise, tell the user to re-input a valid answer and look for the next input
	    	else {
	    		System.out.println("Invalid response. Please type an integer.");
	    		scanner.next();
	    	}
	    }
	    
	    //asking user to input data + creating variable to store that data later
	    System.out.println("What is your instructor's name?");
	    String name = "";
	    
	    //continue to ask the user for input until the user responds properly
	    while(scanner.hasNext()) {
	    	
	    	//if the user enters the correct input, set the variable equal to their input and end the for loop
	    	if(scanner.hasNextDouble() == false && scanner.hasNextInt() == false) {
	    		name = scanner.next();
	    		break;
	    	}
	    	
	    	//otherwise, tell the user to re-input a valid answer and look for the next input
	    	else {
	    		System.out.println("Invalid response. Please type a string.");
	    		scanner.next();	
	    	}
	    }
	    
	    //asking user to input data + creating variable to store that data later
	    System.out.println("What is the class size?");
	    int classSize = 0;
	    
	    //continue to ask the user for input until the user responds properly
	    while(scanner.hasNext()) {
	    	
	    	//if the user enters the correct input, set the variable equal to their input and end the for loop
	    	if(scanner.hasNextInt()) {
	    		classSize = scanner.nextInt();
	    		break;
	    	}
	    	
	    	//otherwise, tell the user to re-input a valid answer and look for the next input
	    	else {
	    		System.out.println("Invalid response. Please type an integer.");
	    		scanner.next();	
	    	}
	    }
	    
	    //final print statements with all the data
	    System.out.println("The course number is " + courseNum);
	    System.out.println("The deparment name is " + deptName);
	    System.out.println("This course meets " + numMeetings + " times per week");
	    System.out.println("This course is " + hours + " hour and " + minutes + " minutes long");
	    System.out.println("The teacher's name is " + name);
	    System.out.println("The class has " + classSize + " students in it");
	}
}